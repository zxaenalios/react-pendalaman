import React from 'react';
import logo from './logo.svg';
import './App.css';
import Navbar from './components/Navbar';
import Clock from './components/Clock';
import Toggle from './components/Toggle';
import Timer from './components/Timer';
import Table from './components/Table';
import Router from './components/Router';



function App() {
  return (
    
    <div className="App">
      <Navbar/>
      <header className="App-header">
        
        <Clock/>
        <Toggle/>
        <Timer/>
        <div className="container">
        <Table/>
        </div>
      </header>
    </div>
  );
}

export default App;
