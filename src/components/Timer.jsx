import React from 'react'

// Show Timer
const Time = function(props) {
	return (
		<h1>
			{props.time}
		</h1>
	);
};

// Reset Timer
const Reset = function(props) {
	return (
		<button type="button" onClick={props.onClickReset} className="btn btn-secondary reset">
			Reset
		</button>
	);
};


// Start/Stop button
class Control extends React.Component {
	constructor(props) {
		super(props);
	};
  
  onClickHandler = () => {
    if(this.props.paused){
      this.props.start();
    }
    else{
      this.props.stop();
    }
  }
  
	render() {
		return (
				<button type="button" className={this.props.paused?"btn btn-primary Stop":"btn btn-danger"} onClick={this.onClickHandler}>
		    	{this.props.paused?"Start":"Stop"}
		    </button>
		);
	};
};


class Timer extends React.Component {
	constructor(props) {
		super(props);
		this.state = { timer: 0, paused: true };
	};
  
  tick = () => {
  	this.setState({ timer : this.state.timer + 1 });
  }
  
	startTimer = () =>{
		this.interval = setInterval(this.tick,1000);
    this.setState({ paused : false });
	}
  
  stopTimer = () => {
  	clearInterval( this.interval );
    this.setState({ paused : true });
	}
  
  reset = () => {
  	this.setState({ timer : 0, paused: true });
    clearInterval( this.interval );
  }
  
	render() {
		return (
			<div>
				<Time time={this.state.timer}  />
        <Control 
          paused={this.state.paused} 
          start={this.startTimer} 
          stop={this.stopTimer} 
        />
        <Reset  onClickReset={this.reset}/>
			</div>
		);
	};
};

export default Timer