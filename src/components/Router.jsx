import React from 'react'
import {BrowserRouter, Route} from 'react-router-dom'
import Navbar from './Navbar'
import Table from './Table'
import App from '../App'

function Router(){
    return (
        <BrowserRouter>
           
            <Route exact path="/" component={App}/>
            <Route exact path="/table" component={Table}/>

        </BrowserRouter>
    )
}

export default Router