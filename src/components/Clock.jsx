import React from 'react'

class Clock extends React.Component{
    constructor(){
        super()
        this.state = {
            tanggal: new Date()
        }
    }

    componentDidMount(){
        setTimeout(()=>{
            this.setState({tanggal: new Date()})
        }, 5000)
    }

    componentDidUpdate(){
        document.getElementById("konten").innerHTML = "Setelah di Update sekarang " + 
        this.state.tanggal.toLocaleTimeString();
    }

    render(){
        return (
            <div>
                <h1> Selamat Pagi </h1>
                <h2> {this.state.tanggal.toLocaleTimeString()}</h2>
                <p id="konten"></p>
            </div>
        )
    }
}

export default Clock