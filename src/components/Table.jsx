import React from 'react'
import Column from './Column'

class Table extends React.Component{
    render(){
        return(
            <table className="table table-dark">
                <tr>
                    <Column/>
                </tr>
            </table>
        )
    }
}

export default Table